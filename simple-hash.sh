#!/bin/bash

# Generate Hash
# $1: Key
# $2: Hash Length
generate() {
	local key=$1
	local -i hashLength=$2
	local -i keyLength=${#key}
	local hash=''
	local -i keyPos=0
	local -i index=0
	while [[ $index -lt $hashLength ]]; do
		local keyChar=${key:$keyPos:1}
		local keyOrd=$(ord $keyChar)
		local number=$((keyPos*953+index*967+keyOrd*971))
		local valueOrd=$((number%83+33))
		local valueChar=$(chr $valueOrd)
		keyPos=$(((number+valueOrd)%keyLength))
		hash="$hash$valueChar"
		((++index))
	done
	echo $hash
}

# Char
chr() {
  [ "$1" -lt 256 ] || return 1
  printf "\\$(printf '%03o' "$1")"
}

# Ordinal
ord() {
  LC_CTYPE=C printf '%d' "'$1"
}

# Main
if [[ $# -ne 2 ]]; then
	echo "Usage: simple-hash KEY LENGTH"
else
	generate "$1" $2
fi