# Simple Hash

A very(!) simple bash tool to generate hashes.

## Usage

```
simple-hash KEY LENGTH
```
